import           Day15                 (sol1, solu)
import           Day18                 (Expr (Lit, Parens, Plus, Times),
                                        interpret, parse, parse2)
import           Test.Hspec            (describe, hspec, it, shouldBe)
import           Test.Hspec.Megaparsec (shouldParse)
import Day21 (Ingr (..), Allergen (..), pLine)
import qualified Text.Megaparsec as TM
main :: IO ()
main = hspec $ do
    describe "day 15" $ do
        it "UT" $ do
                sol1 [0,3,6] `shouldBe` 436
                sol1 [1,3,2] `shouldBe` 1
                sol1 [2,1,3] `shouldBe` 10
                sol1 [1,2,3] `shouldBe` 27
                sol1 [2,3,1] `shouldBe` 78
                sol1 [3,2,1] `shouldBe` 438
                sol1 [3,1,2] `shouldBe` 1836
        it "UT bis" $ do
                solu 2020 [0,3,6] `shouldBe` 436
                solu 2020 [1,3,2] `shouldBe` 1
                solu 2020 [2,1,3] `shouldBe` 10
                solu 2020 [1,2,3] `shouldBe` 27
                solu 2020 [2,3,1] `shouldBe` 78
                solu 2020 [3,2,1] `shouldBe` 438
                solu 2020 [3,1,2] `shouldBe` 1836
    describe "day18" $ do
        it "parser" $ do
            parse "2*3" `shouldParse` Times (Lit 2) (Lit 3)
            parse "(4*5)" `shouldParse` Parens  (Times (Lit 4) (Lit 5))
            parse "2*3+(4*5)" `shouldParse` Plus (Times (Lit 2) (Lit 3)) (Parens $ Times (Lit 4) (Lit 5))
        it "interpretator" $ do
            (interpret <$> parse "1 + 2 * 3 + 4 * 5 + 6") `shouldParse` 71
            (interpret <$> parse2 "1 + 2 * 3 + 4 * 5 + 6") `shouldParse` 231
            (interpret <$> parse2 "1 + (2 * 3) + (4 * (5 + 6))") `shouldParse` 51
            (interpret <$> parse2 "2 * 3 + (4 * 5)") `shouldParse` 46
            (interpret <$> parse2 "5 + (8 * 3 + 9 + 3 * 4 * 3)") `shouldParse` 1445
            (interpret <$> parse2 "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))") `shouldParse` 669060
            (interpret <$> parse2 "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2") `shouldParse` 23340
    describe "day21" $
        it "parser" $ do
            TM.parse pLine "ut" "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)" `shouldParse` (Ingr <$> ["mxmxvkd", "kfcds", "sqjhc", "nhms"], Allergen <$>["dairy", "fish"])


