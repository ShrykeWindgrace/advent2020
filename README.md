# advent2020

My solutions for advent of code 2020 in Haskell

## Usage

```bash
$ stack exec -- advent -- day 1
```
or, more generally,
```bash
$ stack exec -- advent --help
Usage: advent.EXE (-d|--day DAY)
  Prints out some Advent of Code 2020 solutions.

Available options:
  -d,--day DAY             Chose a day to run
  -h,--help                Show this help text
```
