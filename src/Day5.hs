module Day5 (runDay) where
import           Data.List        (sort)
import           Paths_advent2020 (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day5.txt"
  content <- readFile realpath
  let ids = ints2id . entry2ints <$> lines content
  print $ maximum ids
  print $ findHole $ sort ids

-- 858
-- 557

entry2ints :: String -> [Int]
entry2ints [] = []
entry2ints (c:cs) = choice c : entry2ints cs where
    choice z = case z of
        'F' -> 0
        'B' -> 1
        'L' -> 0
        'R' -> 1
        _   -> error "unexpected character"

-- big-endian binary number
ints2id :: [Int] -> Int
ints2id ints = sum $ zipWith (*) ints $ fmap (2^) [9, 8 .. 0]

-- suppose that this list is already sorted
findHole :: [Int] -> Maybe Int
findHole (x : y : rest) = if y - x == 2 then Just $ x + 1 else findHole (y : rest)
findHole _          = Nothing
