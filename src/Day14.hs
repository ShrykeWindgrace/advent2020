{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE StrictData                 #-}
module Day14 (runDay) where
import           Control.Monad.State.Class  (modify)
import           Control.Monad.State.Strict (execState, State, evalState)
import           Data.Bits                  (Bits (clearBit, setBit))
import           Data.Foldable              (traverse_)
import qualified Data.IntMap                as IM
import           Data.List                  (foldl', isPrefixOf)
import           Data.Maybe                 (mapMaybe)
import           Lens.Micro                 (_1, _2, _3)
import           Lens.Micro.Mtl             (use, zoom, (.=), (<<%=))
import           Paths_advent2020           (getDataFileName)

runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day14.txt"
  content <- readFile realpath
  let ords = parseLine <$> lines content
  print $ evalState (navigate logic1) (Mask[], IM.empty, ords)
  print $ evalState (navigate logic2) (Mask[], IM.empty, ords)
  putStrLn "another"
  print $ sm $ snd $ execState (traverse_ (nav logic1) ords) mempty
  print $ sm $ snd $ execState (traverse_ (nav logic2) ords) mempty

  traverse_ print $ (\l -> sm $ snd $ execState (traverse_ (nav l) ords) mempty) <$> [logic1, logic2]

-- 11501064782628
-- 5142195937660
data Bit = Zero | One | F

newtype Mask = Mask [(Int, Bit)] deriving newtype (Semigroup, Monoid)

newtype Addr = Addr Int
    deriving newtype (Bits, Eq)

-- address value
data Command = Command Addr Int

parseBit :: Char -> Maybe Bit
parseBit '1' = Just One
parseBit '0' = Just Zero
parseBit 'X' = Just F
parseBit _   = Nothing

parseMask' :: String -> Mask
parseMask' s = Mask $ mapMaybe (traverse parseBit) $ zip [0::Int ..] $ reverse s

parseCommand :: String -> Command
parseCommand s = let
    [a, _, v] = words s
    addr = Addr $ read $ init $ drop 4 a
    val = read v
    in Command addr val

parseLine :: String -> Either Mask Command
parseLine s
    | "mask = " `isPrefixOf` s =  Left $ parseMask' $ drop 7 s
    | "mem[" `isPrefixOf` s = Right $ parseCommand s
    | otherwise = error $ "parse error " <> show s

applyMaskToVal :: Mask -> Int -> Int
applyMaskToVal (Mask ps) val = foldl' app val ps where
    app :: Int -> (Int, Bit) -> Int
    app acc (pos, One)  = setBit acc pos
    app acc (pos, Zero) = clearBit acc pos
    app acc _           = acc

applyMaskToAddr :: Mask -> Addr -> [Addr]
applyMaskToAddr (Mask ps) addr = foldl' app [addr] ps where
    app :: [Addr] -> (Int, Bit) -> [Addr]
    app acc (_, Zero)  = acc
    app acc (pos, One) = (`setBit` pos) <$> acc
    app acc (pos, F)   = [(`clearBit` pos), (`setBit` pos)] <*> acc


type M = State (Mask, IM.IntMap Int, [Either Mask Command])

type Logic = Command -> Mask -> State (IM.IntMap Int) ()

navigate :: Logic  -> M Int
navigate logic = do
    orders <- _3 <<%= drop 1
    case orders of
        [] -> do
            mem <- use _2
            pure $ sm mem
        (order:_) -> do
            case order of
                Left mask -> _1 .= mask
                Right c -> do
                    mask <- use _1
                    zoom _2 $ logic c mask
            navigate logic

logic1 :: Logic
logic1 (Command (Addr addr) value) mask = do
    let newVal = applyMaskToVal mask value
    modify $ IM.insert addr newVal

logic2 :: Logic
logic2 (Command addr value) mask = do
    let addrs = applyMaskToAddr mask addr
    traverse_ (\(Addr a) -> modify $ IM.insert a value) addrs

sm :: IM.IntMap Int -> Int
sm = IM.foldl' (+) 0


--another approach

nav :: Logic -> Either Mask Command -> State (Mask, IM.IntMap Int) ()
nav logic order = do
    case order of
        Left mask -> _1 .= mask
        Right c -> do
            mask <- use _1
            zoom _2 $ logic c mask
