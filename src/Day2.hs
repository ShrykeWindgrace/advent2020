module Day2 (runDay) where
import           Data.Char        (isDigit)
import           Data.Coerce      (coerce)
import           Data.Semigroup   (Sum (Sum))
import           Data.Tuple.Extra (both)
import           Paths_advent2020 (getDataFileName)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day2.txt"
    content <- readFile realpath
    let parsed = parse <$> lines content
    print $ length $ filter validate1 parsed
    print $ length $ filter validate2  parsed
    print $ single parsed


data Policy = Policy !Int !Int !Char


-- can replace with a proper parser, I think.
-- this one works, too
-- "5-7 n: nnncngdnznjx"
parse :: String -> (Policy, String)
parse s = let
    (fd, r1) = span isDigit s
    (sd, r2) = span isDigit $ tail r1
    ch =  r2 !! 1
    pass = drop 2 r2
  in
    (Policy (read fd) (read sd) ch, pass)

validate1 :: (Policy, String) -> Bool
validate1 (Policy l u c, pass) = let
    len = length $ filter (== c) pass
  in
    l <= len && len <= u

validate2 :: (Policy, String) -> Bool
validate2 (Policy l u c, pass) = let
    c1 = pass !! (l + 1)
    c2 = pass !! (u + 1)
  in
    (c1 == c) /= (c2 == c)

-- can we do it in a single pass?
-- yes, we can!

boolToS :: Bool -> Sum Int
boolToS b = Sum $ if b then 1 else 0

single :: [(Policy, String)] -> (Int, Int)
single = coerce . foldMap (\pair -> both boolToS (validate1 pair, validate2 pair))
