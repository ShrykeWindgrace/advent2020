{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE StrictData       #-}
module Day12 (runDay) where
import           Control.Monad.State        (State, execState)
import           Control.Monad.State.Strict (MonadState (get))
import           Data.Foldable              (traverse_)
import           Lens.Micro                 (Lens', _1, _2)
import           Lens.Micro.Mtl             (use, zoom, (*=), (+=), (.=))
import           Paths_advent2020           (getDataFileName)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day12.txt"
    content <- readFile realpath
    let moves =  parseMove <$> lines content
    print $ manh $ _pos $ execState (traverse_ (runMoves pos) moves) (Ship (1,  0) (0,0))
    print $ manh $ _pos $ execState (traverse_ (runMoves way) moves) (Ship (10, 1) (0,0))
-- 1710
-- 62045


data Move  = Rotate LR Int | Abs Int Int | F Int

data LR = L | R

parseMove :: String ->  Move
parseMove (d:n) = let val = read n :: Int in case d of
    'L' -> Rotate L val
    'R' -> Rotate R val
    'N' -> Abs 0 val
    'S' -> Abs 0 (-val)
    'W' -> Abs (-val) 0
    'E' -> Abs val 0
    'F' -> F val


manh :: (Int, Int) -> Int
manh (x,y) = abs x + abs y

data Ship = Ship {_way :: (Int, Int), _pos :: (Int, Int)}

-- way :: Functor f => ((Int, Int) -> f (Int, Int)) -> Ship -> f Ship
way :: Lens' Ship (Int, Int)
way w2fw (Ship w p) = (`Ship` p) <$> w2fw w

-- pos :: Functor f => ((Int, Int) -> f (Int, Int)) -> Ship -> f Ship
pos :: Lens' Ship (Int, Int)
pos p2fp (Ship w p) = Ship w <$> p2fp p

runMoves :: Lens' Ship (Int,Int) -> Move -> State Ship ()
runMoves who move = do
    case move of
        Abs dx dy -> do
            zoom who $ do
                _1 += dx
                _2 += dy
        F v -> do
            (dx, dy) <- use way
            (x, y) <- use pos
            pos .= (x + v * dx, y + v * dy)
        Rotate d v -> zoom way $ rotate d v

rotate :: LR -> Int -> State (Int, Int) ()
rotate d val = do
    (dx, dy) <- get
    case d of
        L -> do
            case val of
                0 -> pure ()
                360 -> pure ()
                90 -> do
                    _1 .= -dy
                    _2 .=  dx
                180 -> do
                    _1 *= (-1)
                    _2 *= (-1)
                270 -> do
                    _1 .= dy
                    _2 .= -dx
        R -> case val of
            0 -> pure ()
            360 -> pure ()
            90 -> do
                _1 .=  dy
                _2 .= -dx
            180 -> do
                _1 *= (-1)
                _2 *= (-1)
            270 -> do
                _1 .= -dy
                _2 .= dx
