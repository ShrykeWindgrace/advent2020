{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE TupleSections      #-}

module Day11 (runDay) where
import           Data.Foldable    (asum)
import           Data.Map.Strict  (Map)
import qualified Data.Map.Strict  as Map
import           Data.Maybe       (fromMaybe, mapMaybe)
import           Paths_advent2020 (getDataFileName)
import           Utils            (limit)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day11.txt"
    content <- readFile realpath
    let initMP = fromLines $ lines content
    let gridSize = (length $ head $ lines content, length $ lines content) :: Pos
    let lim' = lim initMP
    print $ Map.foldl' (\acc s -> if s == Taken then acc + 1 else acc) 0 lim'

    -- 2299
    let lim2 = limit2 gridSize initMP
    print $ Map.foldl' (\acc s -> if s == Taken then acc + 1 else acc) 0 lim2
-- 2047


type Pos = (Int, Int)
data Seat = Empty | Taken deriving stock Eq



-- parsing

line :: (Int, String) -> [Pos]
line (y, hs) = fmap (,y) $ mapMaybe detectL $ zip [0 .. ] hs where
    detectL :: (a, Char) -> Maybe a
    detectL (a, 'L') = Just a
    detectL _        = Nothing

fromLines :: [String] -> Map Pos Seat
fromLines ls =  Map.fromList $ zip (zip [0..] ls >>= line) (repeat Empty)

iter :: Map Pos Seat -> Map Pos Seat
iter mp = Map.mapWithKey ( \key seat -> let adjs = (\k -> Map.findWithDefault Empty k mp) <$> adj key in case seat of
    Empty -> if all (== Empty) adjs then Taken else seat;
    Taken -> if 4 <= length ( filter (==Taken) adjs) then Empty else seat ) mp


iter2 :: Pos -> Map Pos Seat -> Map Pos Seat
iter2 grid mp = Map.mapWithKey ( \key seat -> let adjs = adjSeen key mp grid in case seat of
    Empty -> if all (== Empty) adjs then Taken else seat;
    Taken -> if 5 <= length ( filter (==Taken) adjs) then Empty else seat ) mp


lim :: Map Pos Seat -> Map Pos Seat
lim = limit iter


limit2 :: Pos -> Map Pos Seat -> Map Pos Seat
limit2 grid = limit (iter2 grid)

adj :: Pos -> [Pos]
adj (x,y) = [(x+1,y), (x-1, y), (x, y+1), (x, y-1), (x+1,y+1), (x-1,y-1), (x+1,y-1), (x-1,y+1)]

adjSeenXP, adjSeenXM, adjSeenYM,adjSeenYP, adjSeenXPYP,adjSeenXPYM,adjSeenXMYP ,adjSeenXMYM:: Pos -> Map Pos Seat -> Pos -> Seat
adjSeenXP (x,y) mp (maxX, _) =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> (,y) <$> [x+1..maxX]
adjSeenXM (x,y) mp _ =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> (,y) <$> [x-1, x-2..0]
adjSeenYM (x,y) mp _ =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> (x,) <$> [y-1, y-2..0]
adjSeenYP (x,y) mp (_, maxY) =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> (x,) <$> [y+1..maxY]

adjSeenXPYP (x,y) mp (maxX,maxY) =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> zip [x+1..maxX] [y+1..maxY]
adjSeenXMYM (x,y) mp _ =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> zip [x-1, x-2..0] [y-1, y-2.. 0]
adjSeenXPYM (x,y) mp (maxX,_) =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> zip [x+1..maxX] [y-1, y-2 .. 0]
adjSeenXMYP (x,y) mp (_,maxY) =  fromMaybe Empty $ asum $ (`Map.lookup` mp) <$> zip [x-1, x-2..0] [y+1..maxY]

adjSeen :: Pos -> Map Pos Seat -> Pos -> [Seat]
adjSeen p mp grid = [adjSeenXP p mp grid, adjSeenYP p mp grid, adjSeenXPYP p mp grid, adjSeenXMYP p mp grid,
                     adjSeenXM p mp grid, adjSeenYM p mp grid, adjSeenXPYM p mp grid, adjSeenXMYM p mp grid]

