module Utils (limit) where

limit :: (Eq a) => (a -> a) -> a -> a
limit step init = let next = step init in if init == next then init else limit step next
