{-# LANGUAGE OverloadedLists  #-}
{-# LANGUAGE TupleSections    #-}
{-# LANGUAGE TypeApplications #-}
module Day4 (runDay) where
import           Data.Char        (isDigit, isSpace)
import           Data.List.Extra  (splitOn, wordsBy)
import           Data.Maybe       (mapMaybe)
import           Data.Set         (member)
import           Paths_advent2020 (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day4.txt"
  content <- readFile realpath
  let entries = wordsBy isSpace <$> splitOn "\n\n" content
  let withValidKeys = filter validateKeys entries
  print $ length withValidKeys
  -- 264
  print  $ length $ filter validateEntry withValidKeys
 -- 224


getKey :: String -> String
getKey = takeWhile (/= ':')

getKV :: String -> Maybe (Field, String)
getKV s = case splitOn ":" s of
    [k,v] -> (, v) <$> k2f k
    _     -> Nothing

keys:: [String]
keys = [
    "byr" ,
    "iyr" ,
    "eyr" ,
    "hgt" ,
    "hcl" ,
    "ecl" ,
    "pid"
  ]

validateKeys :: [String] -> Bool
validateKeys s =  all (\k -> k `elem` (getKey <$> s)) keys

data Field = Byr | Iyr | Eyr | Hgt | Hcl | Ecl | Pid

k2f :: String -> Maybe Field
k2f "byr" = Just Byr
k2f "iyr" = Just Iyr
k2f "eyr" = Just Eyr
k2f "hgt" = Just Hgt
k2f "hcl" = Just Hcl
k2f "ecl" = Just Ecl
k2f "pid" = Just Pid
k2f _     = Nothing

validate :: Field -> String -> Bool
validate Byr s = length s == 4 && "1920" <= s && s <= "2002"

validate Iyr s = let
    l = length s
    val = read @Int s
  in
    l == 4 && 2010 <= val && val <= 2020

validate Eyr s = let
    l = length s
    val = read @Int s
  in
    l == 4 && 2020 <= val && val <= 2030

validate Pid s = let
    l = length s
  in
    l == 9 && all isDigit s

validate Hgt [x,y,z,'c','m'] = let val = read @Int [x,y,z] in 150 <= val && val <= 193
validate Hgt [x,y,'i','n'] = let val = read @Int [x,y] in 59 <= val && val <= 76
validate Hgt _ = False

validate Ecl s = s `member` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
validate Hcl ('#':six) = let len = length six in
    len == 6 && all (`member` (['a'..'f'] <> ['0'..'9'])) six

validate Hcl  _ = False

validateEntry :: [String] -> Bool
validateEntry s = all (uncurry validate) (mapMaybe getKV s)
