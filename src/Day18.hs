{-# LANGUAGE DerivingStrategies #-}
module Day18 (Expr (Lit, Parens, Plus, Times),interpret, parse, parse2, runDay) where
import           Control.Monad.Combinators.Expr (Operator (InfixL),
                                                 makeExprParser)
import           Data.Void                      (Void)
import           Paths_advent2020               (getDataFileName)
import           Text.Megaparsec                (ParseErrorBundle, Parsec,
                                                 between, choice, oneOf,
                                                 runParser, some)
import           Text.Megaparsec.Char           (char, digitChar, string)

runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day18.txt"
    content <- readFile realpath
    let r = parse <$> lines content
    let s = traverse (fmap interpret) r
    print $ sum <$> s

    let r2 = parse2 <$> lines content
    let s2 = traverse (fmap interpret) r2
    print $ sum <$> s2

-- Right 13976444272545
-- Right 88500956630893
data Expr =
    Lit Integer |
    Plus Expr Expr |
    Times Expr Expr |
    Parens Expr
    deriving stock (Eq, Show)

interpret :: Expr -> Integer
interpret (Lit v)       = v
interpret (Plus e1 e2)  = interpret e1 + interpret e2
interpret (Times e1 e2) = interpret e1 * interpret e2
interpret (Parens e)    = interpret e

type Parser = Parsec Void String
type EB = ParseErrorBundle String Void

parseLit :: Parser Expr
parseLit = Lit . read <$> some digitChar

parse :: String -> Either EB Expr
parse s = runParser pExpr "" $ filter (/= ' ' ) s

parse2 :: String -> Either EB Expr
parse2 s = runParser pExpr2 "" $ filter (/= ' ' ) s

binary :: String -> (Expr -> Expr -> Expr) -> Operator Parser Expr
binary name f = InfixL (f <$ string name)

parens :: Parser a -> Parser a
parens = between (char '(' ) (char ')')

pTerm :: Parser Expr
pTerm = choice [Parens <$> parens pExpr, parseLit]

pTerm2 :: Parser Expr
pTerm2 = choice [Parens <$> parens pExpr2, parseLit]

pExpr :: Parser Expr
pExpr = makeExprParser pTerm operatorTable

pExpr2 :: Parser Expr
pExpr2 = makeExprParser pTerm2 operatorTable2

operatorTable :: [[Operator Parser Expr]]
operatorTable = [plus <> times]

operatorTable2 :: [[Operator Parser Expr]]
operatorTable2 = [plus, times]

times, plus :: [Operator Parser Expr]
times = [binary "*" Times]
plus = [binary "+" Plus]
