module Day9 (runDay) where
import           Data.Foldable    (asum)
import qualified Data.IntSet      as IS
import           Data.List        (inits, tails)
import           Data.Maybe       (fromMaybe)
import           Paths_advent2020 (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day9.txt"
  content <- readFile realpath
  let ns = (read <$> lines content) :: [Int]
  let err = findError ns
  print err -- 57195069
  let cont = findContiguous err ns
  print $ maximum cont + minimum cont  -- 7409241

unrepresentable :: Int -> [Int] -> Bool
-- unrepresentable n cs =  null [x + y | x<- cs, y <- cs, x + y == n, x /= y]
-- unrepresentable n cs =  not $ any (\x -> n-x `elem` cs) $ filter (\x -> 2 * x /= n) cs
unrepresentable n cs =  let set = IS.fromList $ filter (\x -> 2 * x /= n) cs in
    IS.foldl' (\acc x -> acc && (n-x) `IS.notMember` set) True set

-- assume that this error is present
findError :: [Int] -> Int
findError ns = let
        (pre, n:_) = splitAt 25 ns
    in
        if unrepresentable n pre then n else findError $ drop 1 ns


-- screw you, RAM and algorithmic complexity
-- also, assume that the solutions exists and is unique

-- (5.83 secs, 13,903,512,872 bytes)
findContiguous :: Int -> [Int] -> [Int]
-- findContiguous n ns = head $ filter (\list -> sum list == n) (concatMap inits $ tails ns) -- (5.83 secs, 13,903,512,872 bytes)
findContiguous n ns = fromMaybe (error "we assume that the solution exists") $ asum $ findInAsc n <$> (inits <$> tails ns) -- (0.31 secs, 1,178,275,184 bytes)

-- assume that lists are in increasing (wrt sum) order
-- this is the case for inits
findInAsc :: Int -> [[Int]] -> Maybe [Int]
findInAsc _ [] = Nothing
findInAsc n (ns:t)
    | s == n = Just ns
    | s > n = Nothing
    | otherwise = findInAsc n t
  where
    s = sum ns
