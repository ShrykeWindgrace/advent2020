module Day7 (runDay) where
import           Control.Monad.RWS.Class  (MonadState (get, put),
                                           MonadWriter (tell), gets)
import           Control.Monad.RWS.Strict (MonadReader (ask), RWS, evalRWS,
                                           unless)
import           Data.List.Extra          (splitOn)
import qualified Data.Map.Strict          as M
import           Data.Maybe               (fromMaybe)
import           Data.Monoid              (Sum (Sum), getSum)
import           Data.Set                 (Set, empty, fromList, notMember,
                                           size, union)
import           Paths_advent2020         (getDataFileName)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day7.txt"
    content <- readFile realpath
    -- suppose that graph is acyclic (a forest)
    let deps = parse =<< lines content
    putStrLn "works 1"
    print $ fst ( evalRWS tick deps (empty, [aim])) - 1 -- because we count shiny gold too in this traversal
    -- should be 254
    let deps2 = parse2 <$> lines content
    print $ getSum $ mappend (Sum (-1)) $ snd $ evalRWS tick2 (M.fromList deps2) [aim2]
    -- 6006



-- light magenta bags contain 3 bright maroon bags, 5 faded yellow bags, 2 dim indigo bags.

newtype Color = Color String deriving (Eq, Ord, Show)

aim :: Color
aim = Color "shiny gold"

aim2 :: (Int, Color)
aim2 = (1, aim)

-- list of "what fits in what"
parse :: String -> [(Color, Color)]
parse s = let
    [l,r] = splitOn "contain" s
    color = Color $ unwords $ take 2 $ words l
    subs = Color . unwords . take 2 . tail  . words <$> splitOn "," r
  in
    (,) <$> subs <*> [color]


tick :: RWS [(Color, Color)] () (Set Color, [Color]) Int
tick = do
    seen <- gets fst
    toTreat_ <- filter (`notMember` seen) <$> gets snd
    case toTreat_ of
        [] -> gets $ size . fst
        toTreat -> do
            global <- ask
            let next = (\c -> [z | (x, z) <- global, x == c]) =<< toTreat
            put (seen `union` fromList toTreat, next)
            tick




-- list of who contains how many of what
parse2 :: String -> (Color, [(Int, Color)])
parse2 s = let
    [l,r] = splitOn "contain" s
    color = Color $ unwords $ take 2 $ words l
    ents = splitOn "," r
    subs = if head (words ( head ents)) == "no" then [] else (\[n, c1,c2,_] -> (read n, Color $ c1 <> " " <> c2) )  . words <$> ents
  in
    (color, subs)

tick2 :: RWS (M.Map Color [(Int, Color)]) (Sum Int) [(Int, Color)] ()
tick2 = do
    toTreat <- get
    unless (null toTreat) $ do
        tell $ Sum $ sum $ fmap fst toTreat
        mp <- ask
        let smth = (\(count, color) -> (\(c', col) -> (c' * count, col)) <$> fromMaybe [] (M.lookup color mp)) =<< toTreat
        put smth
        tick2
