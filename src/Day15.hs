{-# LANGUAGE TypeApplications #-}
module Day15 (runDay, sol1, solu) where
import           Control.Monad.State.Strict (State, evalState, execState, get,
                                             replicateM_)
import qualified Data.IntMap.Strict         as IM
import           Lens.Micro                 (_1, _2, _3, (^.))
import           Lens.Micro.Mtl             (use, (%=), (+=), (.=))
import           Control.DeepSeq

-- to be run with, e.g.
-- $ stack exec -- advent --day 15 +RTS -s -A128m -AL256m -qn8 -N12 -I0
--
-- 30Gb total heap allocs, ~23s total time
runDay :: IO ()
runDay = do

    print @Int $ sol1 [5,1,9,18,13,8,0] -- 376
    print @Int $ sol2 [5,1,9,18,13,8,0] -- 323780 -- incredibly slow...

type MP = IM.IntMap Int
-- last spoken turn per number; phrase; current turn
-- phrase is not yet taken into account
tick :: Int -> State (MP, Int, Int) Int
tick cut = do
    (mp, phrase, turn) <- get

    let nextPhrase = maybe 0 (\t -> turn - t) $ IM.lookup phrase mp

    _1 %= IM.insert phrase turn
    _2 .= nextPhrase

    _3 += 1

    if turn == cut - 1 then use _2 else tick cut

tick2 :: State (MP, Int, Int) ()
tick2 = do
    (mp, phrase, turn) <- get

    let nextPhrase = maybe 0 (\t -> turn - t) $ IM.lookup phrase mp

    --_1 %= ($!!) (IM.insert phrase turn)
    _1 %= IM.insert phrase turn
    _2 .= nextPhrase

    _3 += 1


solu :: Int -> [Int] -> Int
solu cut ints = let
    phrase = last ints
    turn = length ints
    known = init ints
    in  execState (replicateM_ (cut - turn) tick2) (IM.fromList $ zip known [1..] , phrase, turn) ^. _2


sol ::Int -> [Int] -> Int
sol cut ints = let
    phrase = last ints
    turn = length ints
    known = init ints
    in evalState (tick cut) (IM.fromList $ zip known [1..] , phrase, turn)

sol1 :: [Int] -> Int
sol1 = sol 2020

sol2 :: [Int] -> Int
sol2 = solu 30000000
