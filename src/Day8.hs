{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE StrictData       #-}
module Day8 (runDay) where
import           Control.Monad.RWS.Strict (MonadState (get), MonadWriter (tell),
                                           RWS, asks, evalRWS, modify, unless)
import qualified Data.Map.Strict          as M
import           Data.Maybe               (fromJust)
import           Data.Monoid              (Sum (Sum), getSum)
import           Data.Set                 (Set, empty, insert, member)
import           Lens.Micro               (_1, _2)
import           Lens.Micro.Mtl           (zoom, (%=))
import           Paths_advent2020         (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day8.txt"
  content <- readFile realpath
  let commands = M.fromList $ zip [0..] $ parse <$> lines content
  print $ getSum $ snd $ evalRWS tick commands (0, empty) -- 1941
  let nops = selectN commands
  let jumps = selectJ commands
  let nops2jmps = flip (M.adjust (\(Nop v) -> Jmp v)) commands <$> nops
  let jmps2nops = flip (M.adjust (\(Jmp v) -> Nop v)) commands <$> jumps
  let res2 = (\adj -> evalRWS tick2 adj (0, empty)) <$> jmps2nops <> nops2jmps
  print $ getSum . snd <$> filter fst res2 -- 2096. Unique solution

data Command = Nop Int | Acc (Sum Int) | Jmp Int

parse :: String -> Command
parse s = let
    [l, r] = words s
    val = read @Int $ if head r == '+' then drop 1 r else r
  in
    case l of
        "acc" -> Acc $ Sum val
        "nop" -> Nop val
        "jmp" -> Jmp val

tick :: RWS (M.Map Int Command) (Sum Int) (Int, Set Int) ()
tick = do
    (pos, seen) <- get
    unless (pos `member` seen) $ do
        _2 %= insert pos -- modify a part of the state
        command <- asks (fromJust . M.lookup pos) -- bad unwrap, but we are safe here
        zoom _1 $ runCommand command
        tick

-- indices of Nop commands
selectN :: M.Map Int Command -> [Int]
selectN = M.foldMapWithKey (\index com -> [index | isNop com]) where
    isNop (Nop _) = True
    isNop _       = False

-- indices of Jmp commands
selectJ :: M.Map Int Command -> [Int]
selectJ = M.foldMapWithKey (\index com -> [index | isJmp com]) where
    isJmp (Jmp _) = True
    isJmp _       = False


-- False if hangs
tick2 :: RWS (M.Map Int Command) (Sum Int) (Int, Set Int) Bool
tick2 = do
    (pos, seen) <- get
    if pos `member` seen then pure False else do
        _2 %= insert pos -- modify a part of the state

        command_ <- asks (M.lookup pos)
        case command_ of
            Nothing -> pure True -- yay!
            Just command -> do
                zoom _1 $ runCommand command
                tick2


-- handles only positions and accumulator
runCommand :: Command -> RWS a (Sum Int) Int ()
runCommand command = case command of
            Nop _     -> modify succ
            Acc val   -> tell val >> modify succ
            Jmp delta -> modify (+delta)
