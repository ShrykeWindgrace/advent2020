{-# LANGUAGE TypeApplications #-}
module Day10 (runDay) where
import           Control.Applicative (Applicative (liftA2))
import           Data.List           (group, sort)
import           Data.List.Extra     (wordsBy)
import           Paths_advent2020    (getDataFileName)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day10.txt"
    content <- readFile realpath

    let entries = sort $ enhance $ read @Int <$> lines content
    print $ sol1 entries -- 1690

    -- putStrLn "tests"
    -- print $ sol1 small

    -- print $ arrangements small 0 (maximum small)

    let inter =  interestingIntervals  entries
    let bounds = fmap (\lst -> (head lst, last lst)) inter
    let partials = fmap (\l -> arrangements entries (minimum l) (maximum l)) inter
    print $ product partials


diffs :: (Num a, Ord a) => [a] -> [(a, Int)]
diffs js = let
    conseqDiffs = zipWith (flip (-)) js $ tail js
    gr = group $ sort conseqDiffs
  in
    (\g -> (head g, length g)) <$> gr


-- we can not avoid these pairs in our connections
detailedDiffs :: (Eq a, Num a) => [a] -> [(a, a)]
detailedDiffs js = filter (\(x,y) -> x + 3 == y) $ zip js $ tail js

-- I cba with a proper splitting without strings
-- essentially, the following two functions split an asc list into sublists such that bounds of consecutive sublists differ by exactly 3
-- we insert different separators for different diffs of two adjacent integers
spl :: [Int] -> String
spl [] = []
spl [x] = show x
spl (x:y:r) = show x <> (if x + 3 == y then  "|" else " ") <> spl (y:r)

splitIn :: [Int] -> [[Int]]
splitIn ints = fmap (fmap read . words  )  ( wordsBy (== '|') $ spl ints)


interestingIntervals :: [Int] -> [[Int]]
interestingIntervals = filter ((>2) . length) . splitIn


enhance :: [Int] -> [Int]
enhance ints = 0: (3 + maximum ints) : ints

sol1 :: [Int] -> Maybe Int
sol1 ints = let
    ds = diffs ints
    in liftA2 (*) (lookup 1 ds) (lookup 3 ds)

arrangements :: [Int] ->  Int -> Int -> Int
arrangements db lower higher
    | lower == higher = 1
    | lower > higher = 0
    | otherwise = let
    cands = within db lower

    in sum $ (\c -> arrangements db c higher) <$> cands

within :: [Int] -> Int -> [Int]
within ints k = filter (\x -> x -k == 1 || x-k==2 ||x-k==3) ints


-- small test data
small :: [Int]
small = sort $ enhance [16, 10,15,5,1,11,7,19,6,12,4]
