{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Day21  where
import           Control.Applicative.Combinators (sepEndBy1)
import           Control.Monad.State.Strict      (State, execState, unless)
import           Data.Coerce                     (coerce)
import           Data.List                       (delete, foldr, intercalate,
                                                  intersect, sortOn)
import qualified Data.Map.Strict                 as Map
import qualified Data.Set                        as Set
import           Data.Void                       (Void)
import           Lens.Micro                      (_1, _2)
import           Lens.Micro.Mtl                  (use, (%=))
import           Paths_advent2020                (getDataFileName)
import           Text.Megaparsec                 (ParseErrorBundle, Parsec,
                                                  between, parseMaybe, some)
import           Text.Megaparsec.Char            (char, letterChar, space,
                                                  string)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day21.txt"
    content <- readFile realpath
    let Just p = traverse (parseMaybe pLine)  (lines content)

    let allIngrs = p >>= fst

    let danger = fst $ execState tick (Set.empty, collapse p)
    let dangerIngrs = snd `Set.map` danger
    let safe = [ing | ing <- allIngrs, ing `Set.notMember` dangerIngrs]
    print $ length safe
    putStrLn $ intercalate  "," $ fmap (show. snd) $ sortOn fst $ Set.toList danger


type Parser = Parsec Void String
type EB = ParseErrorBundle String Void

newtype Ingr = Ingr String deriving newtype (Eq, Ord)
instance Show Ingr where
    show = coerce

newtype Allergen = Allergen String deriving newtype (Eq, Ord, Show)

type M = Map.Map Allergen [Ingr]

pLine :: Parser ([Ingr], [Allergen])
pLine = do
    preI <- sepEndBy1 (some letterChar ) space
    preA <- between (char '(') (char ')') (string "contains " *> sepEndBy1 (some letterChar) (char ',' >> space))
    pure $ coerce (preI, preA)

insert' :: Allergen -> [Ingr] -> M -> M
insert' a ings = Map.alter f a where
    f :: Maybe [Ingr] -> Maybe [Ingr]
    f Nothing     = Just ings
    f (Just prev) = Just $ intersect prev ings


collapse :: [([Ingr], [Allergen])] -> M
collapse input = foldr g Map.empty (input >>= (\(ings, alls) -> [(a, ings) | a <- alls])) where
    g :: (Allergen, [Ingr]) -> M -> M
    g = uncurry insert'

findL1 :: M -> [(Allergen,Ingr)]
findL1 m = [ (a, i) | (a, [i]) <- Map.toList m]

adapt :: (Allergen,Ingr) -> M -> M
adapt (allg, ing) = Map.mapMaybeWithKey f where
    f key prev = if key == allg then Nothing else Just (delete ing prev)

tick :: State (Set.Set (Allergen, Ingr), M) ()
tick = do
    singles <- findL1 <$> use _2

    unless (null singles) $ do
        _2 %= foldr1 (.) (adapt <$> singles)
        _1 %= (\s -> Set.union s (Set.fromList singles))
        tick


testMap :: M
testMap = Map.fromList [ (Allergen "fst", [Ingr "fstIngr"]), (Allergen "snd", [Ingr "sndIngr"])]
