{-# LANGUAGE TupleSections #-}
module Day17 (runDay) where
import           Control.Monad              (replicateM_)
import           Control.Monad.State.Strict (MonadState (get), State, execState,
                                             modify)
import           Data.Coerce                (coerce)
import           Data.Foldable              (traverse_)
import           Data.List                  (delete)
import qualified Data.Map.Strict            as M
import           Data.Maybe                 (mapMaybe)
import           Data.Semigroup             (Max (Max), Min (Min))
import           Lens.Micro                 (_1, _2, _3)
import           Lens.Micro.Mtl             (use, view, (%=), (.=), (<%=))
import           Paths_advent2020           (getDataFileName)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day17.txt"
    content <- readFile realpath
    print $ part1 content
    print $ part2 content

-- game of life in 3D/4D
-- 319
-- 2324

type Cell = (Int, Int, Int)
type Cell2 = (Int, Int, Int, Int)

type S a = (M.Map a Int, a, a)

nei :: Cell -> [Cell]
nei p@(x,y,z) = delete p $ (,,) <$> [x-1..x+1] <*> [y-1..y+1] <*> [z-1..z+1]
nei2 :: Cell2 -> [Cell2]
nei2 p@(x,y,z,h) = delete p $ (,,,) <$> [x-1..x+1] <*> [y-1..y+1] <*> [z-1..z+1] <*> [h-1..h+1]
--  old -> neigh -> new
rule :: Int -> Int -> Int
rule 1 2 = 1
rule 1 3 = 1
rule 1 _ = 0
rule 0 3 = 1
rule 0 _ = 0

tick :: State (S Cell) ()
tick = do
    (xMin, yMin, zMin) <- use _2
    (xMax, yMax, zMax) <- use _3
    let newSupport = (,,) <$> [xMin-1..xMax+1] <*> [yMin-1..yMax+1] <*> [zMin-1..zMax+1] -- candidate support
    traverse_ (\cell -> _1 %= insertMaybe cell) newSupport
    field <- use _1

    newField <- _1 <%= M.mapWithKey (\cell st -> rule st (sum $ (\k -> M.findWithDefault 0 k field) <$> nei cell))
    let (newMin, newMax) = detectBounds newField
    _2 .= newMin
    _3 .= newMax

tick2 :: State (S Cell2) ()
tick2 = do
    (xMin, yMin, zMin, hMin) <- use _2
    (xMax, yMax, zMax, hMax) <- use _3
    let newSupport = (,,,) <$> [xMin-1..xMax+1] <*> [yMin-1..yMax+1] <*> [zMin-1..zMax+1] <*> [hMin-1..hMax+1] -- candidate support
    traverse_ (\cell -> _1 %= insertMaybe cell) newSupport
    field <- use _1

    newField <- _1 <%= M.mapWithKey (\cell st -> rule st (sum $ (\k -> M.findWithDefault 0 k field) <$> nei2 cell))
    let (newMin, newMax) = detectBounds2 newField
    _2 .= newMin
    _3 .= newMax



insertMaybe :: Ord a => a -> M.Map a Int -> M.Map a Int
insertMaybe cell mp = if cell `M.notMember` mp then M.insert cell 0 mp else mp

detectBounds :: M.Map Cell Int -> (Cell, Cell)
detectBounds = coerce . M.foldMapWithKey (\cell val -> if val == 1 then (toMin cell, toMax cell) else mempty)

    where
        toMax :: Cell -> (Max Int, Max Int, Max Int)
        toMax = coerce
        toMin :: Cell -> (Min Int, Min Int, Min Int)
        toMin = coerce

detectBounds2 :: M.Map Cell2 Int -> (Cell2, Cell2)
detectBounds2 = coerce . M.foldMapWithKey (\cell val -> if val == 1 then (toMin cell, toMax cell) else mempty)

    where
        toMax :: Cell2 -> (Max Int, Max Int, Max Int, Max Int)
        toMax = coerce
        toMin :: Cell2 -> (Min Int, Min Int, Min Int, Min Int)
        toMin = coerce



line :: (Int, String) -> [(Int, Int)]
line (y, hs) = fmap (,y) $ mapMaybe detectL $ zip [0 .. ] hs where
    detectL :: (a, Char) -> Maybe a
    detectL (a, '#') = Just a
    detectL _        = Nothing

fromLines :: [String] -> M.Map Cell Int
fromLines ls =  M.fromList $ zip ((\(x,y) -> (x,y,0)) <$> (zip [0..] ls >>= line)) (repeat 1)

active :: M.Map a Int -> Int
active = M.foldr' (+) 0

part1 :: String -> Int
part1 s  = active $ view _1 $ execState (replicateM_ 6 tick) (field, mini, maxi) where
    field = fromLines $ lines s
    (mini, maxi) = detectBounds field

part2 :: String -> Int
part2 s  = active $ view _1 $ execState (replicateM_ 6 tick2) (field, mini, maxi) where
    field = M.mapKeys (\(x,y,z) -> (x,y,z,0)) $ fromLines $ lines s
    (mini, maxi) = detectBounds2 field
