module Day6 (runDay) where
import           Paths_advent2020 (getDataFileName)
import           Data.Char        (isSpace)
import           Data.List.Extra  (intersect, nubOrd, splitOn, wordsBy)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day6.txt"
    content <- readFile realpath
    let entries = wordsBy isSpace <$> splitOn "\n\n" content
    print $ sum $ fmap uniq entries
    print $ sum $ fmap common entries
-- 6521
-- 3305

uniq :: [String] -> Int
uniq = length . nubOrd . concat

common :: [String] -> Int
common [] = 0
common ss = length $ foldr intersect ['a'..'z'] ss
