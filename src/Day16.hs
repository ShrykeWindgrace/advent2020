{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE StrictData         #-}
module Day16  where
import           Control.Monad.State.Strict (State, evalState, execState,
                                             modify)
import           Data.Foldable              (sequenceA_)
import           Data.List.Extra            (delete, intersect, isPrefixOf,
                                             product, splitOn)
import           Data.Map.Strict            (Map)
import qualified Data.Map.Strict            as M
import           Data.Maybe                 (mapMaybe)
import           Paths_advent2020           (getDataFileName)
import           Text.Read                  (readMaybe)
import           Utils                      (limit)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day16.txt"
    content <- readFile realpath
    let Just (rules, your, their) = parseInput content
    let invalids = findInvalid (concat their) rules
    print $ sum invalids
    print $ sol2 (lim $ itog (rules, your, their)) your

-- 25984
-- 1265347500049

fitRule :: Int -> Rule -> Bool
fitRule t (Rule _ (a, b) (c, d)) = (a <= t && t <= b) || (c <= t && t <= d)

findInvalid :: [Int] -> [Rule] -> [Int]
findInvalid ints rules = [i | i <- ints, not (any (fitRule i) rules)]

data Rule = Rule {name :: String, left :: (Int, Int), right :: (Int, Int)} deriving stock (Show, Eq, Ord)

-- duration: 27-833 or 855-965
parseRule :: String -> Maybe Rule
parseRule s = do
    let [n, t] = splitOn ":" s
    let [l, "or", r] = words t
    Rule n <$> parsePair l <*> parsePair r
    where
        parsePair :: String -> Maybe (Int, Int)
        parsePair p = do
            let [a,b] = splitOn "-" p
            (,) <$> readMaybe a <*> readMaybe b

type Ticket = [Int]

parseTicket :: String -> Maybe Ticket
parseTicket s = traverse readMaybe $ splitOn "," s

parseInput :: String -> Maybe ([Rule], Ticket, [Ticket])
parseInput s = do
    let [rs, your, their] = splitOn "\n\n" s

    rules <- traverse parseRule $ lines rs
    yours <- parseTicket $ lines your !! 1
    theirs <- traverse parseTicket $ drop 1 $ lines their
    pure (rules, yours, theirs)


possible :: Rule -> Ticket -> Map Rule [Int]
possible r ticket = let
    indices = [i | (i, val) <- zip [0..] ticket, fitRule val r]
    in M.singleton r indices

valids :: [Ticket] -> [Rule] -> [Ticket]
valids ts rs = [t | t <- ts, all (\x -> any (fitRule x) rs) t]

itog :: ([Rule] , Ticket, [Ticket]) -> Map Rule [Int]
itog (rs, y, ts) = let
    v = y : valids ts rs
    ps = possible <$> rs <*> v
    in M.unionsWith intersect ps

bump :: Map Rule [Int] -> Map Rule [Int]
bump mp = let
    cand = [purge r x | (r, [x]) <- M.assocs mp]
    in execState (sequenceA_ cand) mp

purge :: Rule -> Int -> State (Map Rule [Int]) ()
purge r x = do
    modify $ M.mapWithKey (\k a -> if k /= r then delete x a else a)


lim :: Map Rule [Int] -> Map Rule [Int]
lim = limit bump

sol2 :: Map Rule [Int] -> Ticket -> Int
sol2 mp ints = let
    indices = mapMaybe (\(r, v) -> if "departure" `isPrefixOf` name r then Just (head v) else Nothing) $ M.assocs mp
    in product $ (ints !!) <$> indices
