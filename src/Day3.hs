{-# LANGUAGE TupleSections #-}
{-# LANGUAGE StrictData    #-}
module Day3 (runDay) where
import           Control.Monad.RWS.Class  (MonadState (get), MonadWriter (tell),
                                           ask, modify)
import           Control.Monad.RWS.Strict (RWS, Sum (..), evalRWS, when)
import           Data.Maybe               (mapMaybe)
import           Data.Set                 (Set)
import qualified Data.Set                 as S
import           Paths_advent2020         (getDataFileName)

-- first part: 292
-- second part: 9354744432
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day3.txt"
    content <- readFile realpath
    let ls = lines content
    let maxY = length ls
    let maxX = length $ head ls
    let set = fromLines ls
    let initPos = (0, 0) :: Pos
    -- first part
    print $ getSum $ snd $ evalRWS runner (set, (maxX, maxY), defaultSlope) initPos
    -- second part
    print $ product $ (\slope -> getSum $ snd $ evalRWS runner (set, (maxX, maxY), slope) initPos) <$> slopes


slopes:: [Slope]
slopes = [Slope 1 1, Slope 3 1, Slope 5 1, Slope 7 1, Slope 1 2]

-- Accumulator would have been better than writer, but I am lazy
runner :: RWS (Set Pos, Pos, Slope) (Sum Int) Pos ()
runner = do
    (set, (maxX, maxY), slope) <- ask
    (currentX, currentY) <- get
    when (currentY < maxY) $ do
        when ((currentX `rem` maxX, currentY) `S.member` set) $ tell (Sum 1)
        modify $ incPos slope
        runner

-- a better way would be to use a Z/nZ sum-monoid for the first coordinate; again, I am lazy
-- something like (Sum (Finite maxX), Sum Int) - and then Slope would have been just a synonym to Pos
-- or better yet, an action on that monoid.
-- mwa-ha-ha
type Pos = (Int, Int)

data Slope = Slope Int Int
defaultSlope :: Slope
defaultSlope = Slope 3 1

incPos :: Slope -> Pos -> Pos
incPos (Slope dx dy) (x, y) = (x + dx, y + dy)


-- parsing

line :: (Int, String) -> [Pos]
line (y, hs) = fmap (,y) $ mapMaybe detectHash $ zip [0 .. ] hs where
    detectHash :: (a, Char) -> Maybe a
    detectHash (a, '#') = Just a
    detectHash _        = Nothing

fromLines :: [String] -> Set Pos
fromLines ls =  S.fromList $ zip [0..] ls >>= line
