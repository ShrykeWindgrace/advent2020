{-# LANGUAGE TypeApplications #-}
module Day1 (runDay) where
import           Control.Monad.State.Class  (get, put)
import           Control.Monad.State.Strict (State, evalState)
import           Data.IntSet                (IntSet)
import qualified Data.IntSet                as IntSet
import           Paths_advent2020           (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day1.txt"
  content <- readFile realpath
  let entries = (read @Int) <$> lines content
  putStrLn $ "First part: " <> show (fp entries)
  putStrLn $ "Second part: " <> show (sp entries)
  print $ evalState runner1 (mempty, entries)

 -- we implicitly suppose that
 -- a) a solution exists (`head` won't explode)
 -- b) this solution is unique
fp, sp :: [Int] -> Int
fp ls =  head $ [x * y | x <- ls, y <- ls, x + y == 2020]
sp ls =  head $ [x * y * z | x <- ls, y <- ls, z <- ls, x + y + z == 2020]

-- Single pass over a list for the first part
runner1 ::  State (IntSet, [Int]) Int
runner1 = do
    (set, incoming) <- get
    case incoming of
      [] -> error "we implicitly suppose that the solution was found before this moment"
      (h:t) -> if (2020 - h) `IntSet.member` set then pure $ (2020 - h) * h else do
          put (h `IntSet.insert` set, t)
          runner1
