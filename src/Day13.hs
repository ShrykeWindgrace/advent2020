{-# LANGUAGE TupleSections    #-}
{-# LANGUAGE TypeApplications #-}
module Day13 (runDay) where
import           Data.List.Extra  (minimumBy, wordsBy)
import           Data.Maybe       (mapMaybe)
import           Data.Ord         (comparing)
import           Paths_advent2020 (getDataFileName)
import           Text.Read        (readMaybe)
runDay :: IO ()
runDay = do
    realpath <- getDataFileName "input_day13.txt"
    ls <- lines <$> readFile realpath
    let (ts, ids) = parse ls
    print $ sol1 (ts, ids)
    let ps = [gcd x y | x<- ids, y <- ids, x /= y]
    print $ all (== 1) ps -- chinese remainder theorem requirement

    let p2 =  parse2 $ ls !! 1

    print $ sol2 p2
    --2947
-- 526090562196173

parse ::[String] -> (Int, [Int])
parse [t, ids] = let
    ts  = read @Int t
    vals = mapMaybe readMaybe $ wordsBy (== ',') ids
    in (ts, vals)

sol1 ::(Int, [Int]) -> Int
sol1  (ts, ids) = let
    resi = (\i -> (i - ts `rem` i, i)) <$> ids
    (a, b) = minimumBy (comparing fst) resi
    in a * b
--
--part 2

-- id, offset
parse2 :: String -> [(Int, Int)]
parse2 s = let
    ss = zip (wordsBy (== ',') s) [0..]
    fn = \(w, off) -> (,off) <$> readMaybe w
    in mapMaybe fn ss



-- chinese remainder theorem with a slight twist
sol2 :: [(Int, Int)] -> Int
sol2 ps = let
    ps' = fmap (\(id, off) -> (id, id - (off `rem` id))) ps
    ai = fmap fst ps'
    ri = fmap snd ps'

    m = product ai
    mi = (\a -> m `quot` a) <$> ai
    miinv = [inv n val | (n, val) <- zip ai mi]
    in sum ( zipWith3 (\a b c -> a * b * c) ri mi miinv) `rem` m

inv :: Int -> Int -> Int
inv n val = head $ filter (\c -> (c * val) `rem` n == 1) [0..n-1]
