module Day20 (runDay) where
import           Paths_advent2020           (getDataFileName)
runDay :: IO ()
runDay = do
  realpath <- getDataFileName "input_day20.txt"
  content <- readFile realpath
  putStrLn "not implemented"
