module Main where

import qualified Day1
import qualified Day2
import qualified Day3
import qualified Day4
import qualified Day5
import qualified Day6
import qualified Day7
import qualified Day8
import qualified Day9
import qualified Day10
import qualified Day11
import qualified Day12
import qualified Day13
import qualified Day14
import qualified Day15
import qualified Day16
import qualified Day17
import qualified Day18
import qualified Day19
import qualified Day20
import qualified Day21
import qualified Day22
import qualified Day23
import qualified Day24
import qualified Day25
import           Options.Applicative (Parser, auto, execParser, fullDesc, help,
                                      helper, info, long, metavar, option,
                                      progDesc, short, (<**>))

main :: IO ()
main = performDay =<< execParser opts
  where
    opts =
      info
        (confParser <**> helper)
        (fullDesc <> progDesc "Prints out some Advent of Code 2020 solutions.")

newtype Config = Config Int

confParser :: Parser Config
confParser = Config <$> option auto (long "day" <> short 'd' <> metavar "DAY" <> help "Chose a day to run")

performDay :: Config -> IO ()
performDay (Config 1)  = Day1.runDay
performDay (Config 2)  = Day2.runDay
performDay (Config 3)  = Day3.runDay
performDay (Config 4)  = Day4.runDay
performDay (Config 5)  = Day5.runDay
performDay (Config 6)  = Day6.runDay
performDay (Config 7)  = Day7.runDay
performDay (Config 8)  = Day8.runDay
performDay (Config 9)  = Day9.runDay
performDay (Config 10) = Day10.runDay
performDay (Config 11) = Day11.runDay
performDay (Config 12) = Day12.runDay
performDay (Config 13) = Day13.runDay
performDay (Config 14) = Day14.runDay
performDay (Config 15) = Day15.runDay
performDay (Config 16) = Day16.runDay
performDay (Config 17) = Day17.runDay
performDay (Config 18) = Day18.runDay
performDay (Config 19) = Day19.runDay
performDay (Config 20) = Day20.runDay
performDay (Config 21) = Day21.runDay
performDay (Config 22) = Day22.runDay
performDay (Config 23) = Day23.runDay
performDay (Config 24) = Day24.runDay
performDay (Config 25) = Day25.runDay
performDay _          = error "index out of bounds"
